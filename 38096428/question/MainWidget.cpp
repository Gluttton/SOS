#include "MainWidget.h"
#include <QQuickView>
#include <QHBoxLayout>
#include <QDebug>

MainWidget::MainWidget (QWidget * parent)
            : QWidget (parent)
{
    setWindowTitle (tr ("QtQuick key press event propagation") );

    QQuickView * view = new QQuickView ();
    view->setSource (QUrl ("qrc:///widget.qml") );
    view->setResizeMode (QQuickView::SizeRootObjectToView);
    QWidget * container = QWidget::createWindowContainer (view, this);
    QVBoxLayout * topLayout = new QVBoxLayout;
    topLayout->addWidget (container);
    setLayout (topLayout);
}

void MainWidget::keyPressEvent (QKeyEvent *)
{
    qDebug () << __FUNCTION__;
}
