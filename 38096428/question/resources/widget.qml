import QtQuick 2.0
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1

Rectangle {
    id: root
    RowLayout {
        anchors.fill: parent
        Item {
            Layout.fillWidth:  true
            Layout.fillHeight: true
            TextField {
                id: textFiled
                anchors.centerIn: parent
            }
        }
    }
}
