#ifndef QMRE_MAIN_WIDGET_H
#define QMRE_MAIN_WIDGET_H

#include <QWidget>
#include <QItemDelegate>
#include <QLineEdit>

class MainWidget : public QWidget
{
Q_OBJECT
public:
    explicit MainWidget     (QWidget * parent = nullptr);
    ~MainWidget             () override = default;
    bool                    isEmpty;
    int                     emptyRow;
};

class ItemDelegate : public QItemDelegate
{
Q_OBJECT
signals:
    void cellEdited (int) const;
public:
    void setModelData (QWidget * widget, QAbstractItemModel * model, const QModelIndex & index) const override
    {
        if (0 == index.column () ) {
            if (QLineEdit * cellWidget = qobject_cast <QLineEdit *> (widget) ) {
                if (cellWidget->text ().isEmpty () ) {
                    emit cellEdited (index.row () );
                    return;
                }
            }
        }

        QItemDelegate::setModelData (widget, model, index);
    }
};

#endif//QMRE_WIDGET_H
