#include "MainWidget.h"
#include <QHBoxLayout>
#include <QApplication>
#include <QMouseEvent>
#include <QGraphicsScene>


MainWidget::MainWidget (QWidget * parent)
            : QWidget (parent)
{
    setWindowTitle (tr ("Qt Property Animation issue") );

    bar = new BarWidget ();

    view = new QGraphicsView (new QGraphicsScene (), this);
    view->setMinimumSize (350 + bar->FullWidth (), 350);
    view->scene ()->setBackgroundBrush (QBrush (QColor (40, 80, 40, 255) ) );
    view->setHorizontalScrollBarPolicy (Qt::ScrollBarAlwaysOff);
    view->setVerticalScrollBarPolicy   (Qt::ScrollBarAlwaysOff);
    view->scene ()->addWidget (bar);

    qApp->installEventFilter (this);

    QHBoxLayout * layoutTop = new QHBoxLayout ();
    layoutTop->addWidget (view);
    setLayout (layoutTop);

    bar->Hide (view->mapToScene (QRect (view->viewport ()->width (), 0, bar->FullWidth (), view->viewport ()->height () ) ).boundingRect () );
}

bool MainWidget::eventFilter (QObject * object, QEvent * event)
{
    if (object == view->viewport () ) {
        const auto wb = bar->FullWidth ();
        const auto vpWidth  = view->viewport ()->width  ();
        const auto vpHeight = view->viewport ()->height ();

        if (QEvent::MouseMove == event->type () ) {
            const auto x = static_cast <QMouseEvent *> (event)->pos ().x ();
            if (x > vpWidth - wb && bar->IsHidden () ) {
                bar->Show (view->mapToScene (QRect (vpWidth - wb, 0, wb, vpHeight) ).boundingRect (), true);
            }
            else if (x < vpWidth - wb && !bar->IsHidden () && !bar->IsFixed () ) {
                bar->Hide (view->mapToScene (QRect (vpWidth,      0, wb, vpHeight) ).boundingRect (), true);
            }
        }
        else if (QEvent::Resize == event->type () || QEvent::Show == event->type () ) {
            if (bar->IsHidden () ) {
                bar->Hide (view->mapToScene (QRect (vpWidth,      0, wb, vpHeight) ).boundingRect () );
            }
            else {
                bar->Show (view->mapToScene (QRect (vpWidth - wb, 0, wb, vpHeight) ).boundingRect () );
            }
        }
    }


    return false;
}

