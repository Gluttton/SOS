#include "MainWidget.h"
#include <QPushButton>
#include <QVBoxLayout>
#include <QGroupBox>


MainWidget::MainWidget (QWidget * parent)
            : QWidget (parent)
{
    setWindowTitle (tr ("QGropBox Title Alignment issue") );

    QGroupBox   * group = new QGroupBox ("Group Title");
    QVBoxLayout * layoutTop   = new QVBoxLayout ();
    QVBoxLayout * layoutGroup = new QVBoxLayout ();
    layoutGroup->addWidget (new QPushButton ("Button A") );
    layoutGroup->addWidget (new QPushButton ("Button B") );
    layoutGroup->addWidget (new QPushButton ("Button C") );
    group->setLayout (layoutGroup);
    layoutTop->addWidget (group);
    group->setAlignment (Qt::AlignHCenter);
    this->setLayout (layoutTop);
}
