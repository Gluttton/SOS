#include <iostream>
#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <fcntl.h>
#include <thread>

int main (int argc, char ** argv)
{
    const auto work = [](const char * path) {
        int file = open (path, O_RDONLY);
        char b;

        while (read (file, & b, 1) > 0) {
            std::cout << b;
        }
        std::cout << strerror (errno) << std::endl;
    };

    std::thread workThread (work, argv [1]);
    workThread.join ();

    return EXIT_SUCCESS;
}
